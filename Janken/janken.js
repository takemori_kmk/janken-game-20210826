
// 規定値を定義
var HAND_TYPE_GU = 0;
var HAND_TYPE_CHOKI = 1;
var HAND_TYPE_PA = 2;
var PLAYER_TYPE_WIN = 0;
var PLAYER_TYPE_LOSE = 1;
var PLAYER_TYPE_DRAW = 2;
var MESSAGE_TYPE_WIN = "勝った！";
var MESSAGE_TYPE_LOSE = "負けた！";
var MESSAGE_TYPE_DRAW = "あいこ！";

// 定数でクラスを定義
var JANKEN_IMG_GU = "janken_gu";
var JANKEN_IMG_CHOKI = "janken_choki";
var JANKEN_IMG_PA = "janken_pa";
var PLAYER_IMG_NORMAL = "normal";
var PLAYER_IMG_WIN = "win";
var PLAYER_IMG_LOSE = "lose";
var PLAYER_IMG_DRAW = "draw";

var win_number = 0;
var lose_number = 0;

$(function () {
    $('#maru-player1').hide();
    $('#maru-player2').hide();
    $('#maru-player3').hide();
    $('#maru-enemy1').hide();
    $('#maru-enemy2').hide();
    $('#maru-enemy3').hide();
});

/**
 * じゃんけんのメイン処理
 */
function jankenStart() {

    // 選択した手を取得
    var myHandType; // 自分の手タイプ
    var myHandEle = document.getElementsByName("hand");
    //myHandEleはinput属性の3要素の配列になっている
    for (var i = 0; i < myHandEle.length; i++) {
        if (myHandEle[i].checked) {
            myHandType = myHandEle[i].value;
        }
    }

    //コンソールに出力して確認（デバッグ）
    console.log(myHandEle);
    console.log(myHandType);

    // 相手の手を決める
    var enemyHandType; // 相手の手タイプ
    var random = Math.floor(Math.random() * 3);
    console.log(random);
    enemyHandType = random;

    // じゃんけん判定
    var msg; // 中央に表示する勝敗メッセージ
    var myType;  // 自分の勝敗タイプ
    var enemyType; // 相手の勝敗タイプ
    if (myHandType == HAND_TYPE_GU) {
        if (enemyHandType == HAND_TYPE_GU) {
            msg = MESSAGE_TYPE_DRAW;
            myType = PLAYER_TYPE_DRAW;
            enemyType = PLAYER_TYPE_DRAW;
        } else if (enemyHandType == HAND_TYPE_PA) {
            msg = MESSAGE_TYPE_LOSE;
            myType = PLAYER_TYPE_LOSE;
            enemyType = PLAYER_TYPE_WIN;
            lose_number += 1;

        } else if (enemyHandType == HAND_TYPE_CHOKI) {
            msg = MESSAGE_TYPE_WIN;
            myType = PLAYER_TYPE_WIN;
            enemyType = PLAYER_TYPE_LOSE;
            win_number += 1;

        }
    } else if (myHandType == HAND_TYPE_PA) {
        if (enemyHandType == HAND_TYPE_GU) {
            msg = MESSAGE_TYPE_WIN;
            myType = PLAYER_TYPE_WIN;
            enemyType = PLAYER_TYPE_LOSE;
            win_number += 1;

        } else if (enemyHandType == HAND_TYPE_PA) {
            msg = MESSAGE_TYPE_DRAW;
            myType = PLAYER_TYPE_DRAW;
            enemyType = PLAYER_TYPE_DRAW;
        } else if (enemyHandType == HAND_TYPE_CHOKI) {
            msg = MESSAGE_TYPE_LOSE;
            myType = PLAYER_TYPE_LOSE;
            enemyType = PLAYER_TYPE_WIN;
            lose_number += 1;

        }
    } else if (myHandType == HAND_TYPE_CHOKI) {
        if (enemyHandType == HAND_TYPE_GU) {
            msg = MESSAGE_TYPE_LOSE;
            myType = PLAYER_TYPE_LOSE;
            enemyType = PLAYER_TYPE_WIN;
            lose_number += 1;

        } else if (enemyHandType == HAND_TYPE_PA) {
            msg = MESSAGE_TYPE_WIN;
            myType = PLAYER_TYPE_WIN;
            enemyType = PLAYER_TYPE_LOSE;
            win_number += 1;

        } else if (enemyHandType == HAND_TYPE_CHOKI) {
            msg = MESSAGE_TYPE_DRAW;
            myType = PLAYER_TYPE_DRAW;
            enemyType = PLAYER_TYPE_DRAW;
        }
    }

    if (win_number == 1) {
        $('#maru-player1').show();
    } else if (win_number == 2) {
        $('#maru-player2').show();
    } else if (win_number == 3) {
        $('#maru-player3').show();
    }

    if (lose_number == 1) {
        $('#maru-enemy1').show();
    } else if (lose_number == 2) {
        $('#maru-enemy2').show();
    } else if (lose_number == 3) {
        $('#maru-enemy3').show();
    }

    $("#win-number").html(win_number + "勝");
    $("#lose-number").html(lose_number + "敗");


    // 表示画像を切り替える
    playerChange("my_player", myType);
    handChange("my_hand", myHandType);
    playerChange("enemy_player", enemyType);
    handChange("enemy_hand", enemyHandType);

    // メッセージ表示
    document.getElementById("message").innerHTML = msg;

}

/**
 * 手の画像を入れ替える
 * @param {String} id 画像を入れ替えるHTML要素のID名
 * @param {Number} type 入れ替える画像の区分
 */
function handChange(id, type) {

    // 要素取得
    var ele = document.getElementById(id);
    console.log(ele);

    // クラス初期化
    ele.classList.remove(JANKEN_IMG_GU);
    ele.classList.remove(JANKEN_IMG_CHOKI);
    ele.classList.remove(JANKEN_IMG_PA);

    console.log(ele.classList);
    // 規定値に応じて、クラスを指定する
    if (type == HAND_TYPE_GU) {
        ele.classList.add(JANKEN_IMG_GU);
    } else if (type == HAND_TYPE_CHOKI) {
        ele.classList.add(JANKEN_IMG_CHOKI);
    } else if (type == HAND_TYPE_PA) {
        ele.classList.add(JANKEN_IMG_PA);
    }
    console.log(ele.classList);
}

/**
 * プレイヤーの画像を入れ替える
 * @param {String} id 画像を入れ替えるHTML要素のID名
 * @param {Number} type 入れ替える画像の区分
 */
function playerChange(id, type) {

    // 要素取得
    var ele = $('#' + id);

    // クラス初期化
    ele.removeClass(PLAYER_IMG_WIN);
    ele.removeClass(PLAYER_IMG_LOSE);
    ele.removeClass(PLAYER_IMG_DRAW);

    // 規定値に応じて、クラスを指定する
    if (type == PLAYER_TYPE_WIN) {
        ele.addClass(PLAYER_IMG_WIN);
    } else if (type == PLAYER_TYPE_LOSE) {
        ele.addClass(PLAYER_IMG_LOSE);
    } else if (type == PLAYER_TYPE_DRAW) {
        ele.addClass(PLAYER_IMG_DRAW);
    }

}
